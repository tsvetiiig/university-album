(() => {
	"use strict"

	const registerForm = document.getElementById('register-form');
	const loginForm = document.getElementById('login-form');
	const logoutBtn = document.getElementById('logout');
	const specialtySelect = document.getElementById('specialty');
	const errors = document.getElementById('errors');
	const urlParams = new URLSearchParams(window.location.search);
	const menuToggle = document.getElementById('toggle');
	const profileName = document.getElementById("profile-name");

	if (urlParams.get("error") === "accessDenied") {
		errors.classList.add('errors-visible');
		errors.innerText = "Access denied. Please login.";
	}

	registerForm && registerForm.addEventListener('submit', event => {
		const formData = new FormData(event.target);
		const name = formData.get('name');
		const email = formData.get('email');
		const password = formData.get('password');
		const password_confirm = formData.get('password_confirm');
		const specialty = formData.get('specialty');
		const phoneNum = formData.get('phone_number');

		event.preventDefault();
		
		if(isValidRegistrationData(name, email, password, password_confirm, specialty, phoneNum)){
			auth.register(name, email, password, specialty, phoneNum, (success, errorCode, errorMessage) => {
				if (success) {
					auth.saveUserData(name, email, specialty, phoneNum, () => window.location = 'home.html');
				} else {
					errors.classList.add('errors-visible');
					errors.innerText = errorMessage;
				}
			});
		}
	});

	loginForm && loginForm.addEventListener('submit', event => {
		const formData = new FormData(event.target);
		const email = formData.get('email');
		const password = formData.get('password');

		event.preventDefault();

		if(isValidLoginData(email, password)){
			auth.login(email, password, (success, errorCode, errorMessage) => {
				if (success) {
					window.location = 'home.html';
				} else {
					errors.classList.add('errors-visible');
					errors.innerText = errorMessage;
				}
			});
		}
	});

	logoutBtn && logoutBtn.addEventListener('click', event => {
		auth.logout();
		window.location = 'index.html';
		event.preventDefault();
	});
	
	firebase.auth().onAuthStateChanged(user => {
		if (user && profileName) {
			profileName.innerText = user.displayName;
		}
	});
		
	const isValidRegistrationData = (name, email, pass, pass_confirm, specialty, phoneNum) => {
		if(!name){
			document.getElementById('errors').innerHTML = 'Name is required.<br>';
			return false;
		}
		
		if(!email){
			document.getElementById('errors').innerHTML = 'Email is required.<br>';
			return false;
		}
		
		if(!email.includes('@') || !email.includes('.')){
			document.getElementById('errors').innerHTML = "Email must contain both '@' and '.'.<br>";
			return false;
		}
		
		if(!pass || !pass_confirm){
			document.getElementById('errors').innerHTML = 'Both password and confirmation password are required.<br>';
			return false;
		}
		
		var passRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
		if(!passRegex.test(pass)){
			document.getElementById('errors').innerHTML = "Password is invalid. It must containt at least one upper case letter, one number and to be longer at least 6 symbols.<br>";
			return false;
		}
		
		if(pass !== pass_confirm){
			document.getElementById('errors').innerHTML = 'Passwords do not match.<br>';
			return false;
		}
		
		if(!specialty){
			document.getElementById('errors').innerHTML = 'Specialty is required.<br>';
			return false;
		}
		
		var specialties = ['computer-science', 'informatics', 'information-systems', 'software-engineering'];
		if(!specialties.includes(specialty)){
			document.getElementById('errors').innerHTML = 'Specialty is invalid.<br>';
			return false;
		}
		
		var phoneRegex = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im;
		if(phoneNum && !phoneRegex.test(phoneNum)){
			document.getElementById('errors').innerHTML = "Phone number is invalid.<br>";
			return false;
		}
		
		return true;
	};
	
	const isValidLoginData = (email, pass) => {
		if(!email){
			document.getElementById('errors').innerHTML = 'Email is required.<br>';
			return false;
		}
		
		if(!email.includes('@') || !email.includes('.')){
			document.getElementById('errors').innerHTML = "Email must contain both '@' and '.'.<br>";
			return false;
		}
		
		if(!pass){
			document.getElementById('errors').innerHTML = 'Password is required.<br>';
			return false;
		}
		
		var passRegex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,}$/;
		if(!passRegex.test(pass)){
			document.getElementById('errors').innerHTML = "Password is invalid. It must containt at least one upper case letter, one number and to be longer at least 6 symbols.<br>";
			return false;
		}
		
		return true;
	};
	
	specialtySelect && specialtySelect.addEventListener('change', event => {
		specialtySelect.style.color = "#000";
	});
	
	menuToggle && menuToggle.addEventListener('click', event => {
		var headerMenu = document.getElementById('header');
		if(headerMenu.classList.contains('menu-toggle-show')){
			headerMenu.classList.remove('menu-toggle-show');
		} else {
			headerMenu.classList.add('menu-toggle-show');
		}
	});

})(this);