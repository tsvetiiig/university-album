(() => {
	"use strict"
	
	const initDatabase = () => {
		const config = {
			apiKey: "AIzaSyB8PKpH30fo-IlchbbgPqAH-BmhrZ1Qc28",
			authDomain: "front-end-web-1a728.firebaseapp.com",
			databaseURL: "https://front-end-web-1a728.firebaseio.com",
			projectId: "front-end-web-1a728",
			storageBucket: "front-end-web-1a728.appspot.com",
			messagingSenderId: "37482172753",
			appId: "1:37482172753:web:e687c1d3913ab1ac8e8370"
		};
		firebase.initializeApp(config);
	};
	initDatabase();

	const login = (email, password, callback) => {
		return firebase.auth().signInWithEmailAndPassword(email, password).then(() => {
			callback(true);
		}, (error) => {
			const errorCode = error.code;
			const errorMessage = error.message;
			callback(false, errorCode, errorMessage);
		});
	};

	const logout = () => {
		return firebase.auth().signOut();
	};
	
	
	const saveUserData = (name, email, specialty, phoneNum, callback) => {
		const currentUser = firebase.auth().currentUser;
		const userId = currentUser ? currentUser.uid : "";
		const usersDbRef = firebase.database().ref('users/' + specialty);
		
		usersDbRef.push({
			'userId': userId,
			'name': name,
			'email': email,
			'specialty': getSpecialty(specialty),
			'phoneNum': (phoneNum ? phoneNum : '-')
		}, error => {
			if (error)
				console.log('Error has occured during saving process')
			else
				callback();
		});
	};
	
	const register = (username, email, password, specialty, phoneNum, callback) => {
		firebase.auth().createUserWithEmailAndPassword(email, password).then((data) => {
			data.user.updateProfile({
				displayName: username
			}).then(function () {
				callback(true);
			});
		}, (error) => {
			const errorCode = error.code;
			let errorMessage;

			switch (errorCode) {
				case 'auth/weak-password':
					{
						errorMessage = "Registration failed. Weak password.";
						break;
					}
				case 'auth/email-already-in-use':
					{
						errorMessage = "Registration failed. Email is already used.";
						break;
					}
				case 'auth/invalid-email':
					{
						errorMessage = "Registration failed. Email is not valid.";
						break;
					}
				default:
					{
						errorMessage = "Registration failed.";
					}
			}

			callback(false, errorCode, errorMessage);
		});
	};
	
	function getSpecialty(specialty){
		return specialty.replace('-', ' ').split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
	};

	this.auth = {
		login: login,
		logout: logout,
		register: register,
		saveUserData: saveUserData
	};
})(this);