(() => {
	"use strict"
	
	const cs = document.getElementById('cs');
	const inf = document.getElementById('inf');
	const is = document.getElementById('is');
	const se = document.getElementById('se');
	const allStudents = document.getElementById('all');
	const selectedSpecialtyName = document.getElementById('specialty-name');
	const students = document.getElementById('students');
	
	const studentHtml = data => {
		const state = data.val();
		
		return `<figure class="profile-avatar students-section-avatar">
					<img src="images/avatar.png" alt="BP" class="profile-image">
				</figure>
				<div class="student-info">
				  <div class="student-profile">
					  <h3 class="student-profile-name">${state.name}</h3>
				  </div>
				  <div class="student-details">
					<div class="student-detail">
						<h4 class="student-detail-title">Specialty:</h4>
						<p class="student-detail-info"><i>${state.specialty}</i></p>
					</div>
					<div class="student-detail">
						<h4 class="student-detail-title">E-mail:</h4>
						<p class="student-detail-info"><i>${state.email}</i></p>
					</div>
					<div class="student-detail">
						<h4 class="student-detail-title">Phone number:</h4>
						<p class="student-detail-info"><i>${state.phoneNum}</i></p>
					</div>
				  </div>
				</div>`;
	}
		
	// Dynamic change of specialty categories
	allStudents.addEventListener('click', () => toggleStudents('All students', 'all'));
	cs.addEventListener('click', () => toggleStudents('Computer Science', 'computer-science'));
	inf.addEventListener('click', () => toggleStudents('Informatics', 'informatics'));
	is.addEventListener('click', () => toggleStudents('Information Systems', 'information-systems'));
	se.addEventListener('click', () => toggleStudents('Software Engineering', 'software-engineering'));
	
	toggleStudents('All students', 'all');
	
	function toggleStudents(specialtyFullName, specialty){
		students.innerHTML = '';
		selectedSpecialtyName.innerHTML = specialtyFullName;
		
		if(specialty === 'all'){
			listAllStudents();
		} else {
			listStudentsFromSpecialty(specialty);
		}
	}
	
	function listAllStudents(){		
		var specialties = ['computer-science', 'informatics', 'information-systems', 'software-engineering'];
		specialties.forEach(specialty => {
			listStudentsFromSpecialty(specialty);
		});
	};
	
	function listStudentsFromSpecialty(specialty){		
		/*if (!validateUser()) {
			return;
		}*/
		
		var studentsRef = firebase.database().ref('/users/' + specialty);
		studentsRef.once('value').then(function(data) {
			data.forEach(function(student) {             
				document.getElementById("loader").classList.add("hidden");
				let div = document.createElement('DIV');
				div.classList.add('student-container');
				div.innerHTML = studentHtml(student);
				students.prepend(div);
			});
		});
	};
				
	function validateUser() {
		if (!firebase.auth().currentUser) {
			window.location = 'index.html?error=accessDenied';
			return false;
		}

		return true;
	};
})();